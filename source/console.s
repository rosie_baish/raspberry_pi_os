.equ NUM_COLS, 85
.equ NUM_ROWS, 48


.section .data
.globl console_write_line
console_write_line:
// r0 is pointer to start of data
// Reads until it finds either a null
// New lines (0x0a) are accepted and will cause a new line to be written
// Stores only the last 48 lines
// If a line is longer than 85 chars it will wrap
push {lr}
bl console_write_word
ldr r0,=Console
ldr r1,=Console_Row
ldr r1,[r1]
mov r2,#NUM_COLS
mla r0,r1,r2,r0
ldr r2,=Console_Col
ldr r2,[r2]
bl pad_rest_of_line
bl increment_line
pop {pc}


.globl console_write_word
console_write_word:
push {lr}
push {r0}
console .req r0
ldr console,=Console
row .req r1
ldr r1,=Console_Row
ldr row,[r1]
mov r2,#NUM_COLS
mla console, row,r2,console
.unreq row
data .req r1
pop {data}
col .req r2
ldr r3,=Console_Col
ldr col,[r3]
char .req r3
console_copy_loop:
ldrb char,[data]
strb char,[console,col]
add data,#1
add col,#1
cmp char,#0
beq null_found
cmp char,#0x0a
beq new_line
cmp col,#85
blge increment_line
b console_copy_loop
null_found:
.unreq console
ldr r0,=Console_Col
str col,[r0]
.unreq col
pop {pc}


new_line:
bl pad_rest_of_line
bl increment_line
b console_copy_loop

pad_rest_of_line:
console .req r0
col .req r2
char .req r3
mov char,#0x20 // Space
pad_rest_of_line_loop:
cmp col,#85
movge pc,lr
strb char,[console,col]
add col,#1
b pad_rest_of_line_loop

increment_line:
// On entry, r0 is console
// r1 = data
// r2 = col
// r3 = char
// Need to preserve data
// col will be set to 0
// Console will be incremented
// Char doesn't matter
push {lr}
push {data}
console .req r0
row_ptr .req r2
row .req r3
ldr row_ptr,=Console_Row		//
ldr row,[row_ptr]
add row,#1
cmp row,#NUM_ROWS
movge row,#0
str row,[row_ptr]
.unreq row_ptr
ldr console,=Console
num_cols .req r2
mov num_cols,#NUM_COLS
mla console, row,num_cols,console
.unreq num_cols
.unreq row
ldr r3,=Console_Col
col .req r2
mov col,#0
str col,[r3]
bl pad_rest_of_line			// Blank out the bottom line in preparation for writing. This is becuase sometimes an entire line is not written at once
mov col,#0
.unreq col
.unreq console
pop {data}
pop {pc}




/* r0 is column, r1 is row, r2 is value */
.globl console_write_hex
console_write_hex:
push {r0,r1,r2,r3,r4,lr}
push {r0}
ldr r0,=s_0x
bl console_write_word

console .req r0
ldr console,=Console
row .req r1
ldr r1,=Console_Row
ldr row,[r1]
mov r2,#NUM_COLS
mla console, row,r2,console
.unreq row
col .req r2
ldr r2,=Console_Col
ldr col,[r2]
sub col,#1		// To avoid space after the '0x'

value .req r1
pop {value}

hex .req r4	// Pointer to hex to ascii converter
ldr hex,=hexToAscii

char .req r3
and char,value,#0xF0000000
lsr char,#28
ldrb char,[hex,char]
strb char,[console,col]
add col,#1
cmp col,#85
bleq increment_line


and char,value,#0x0F000000
lsr char,#24
ldrb char,[hex,char]
strb char,[console,col]
add col,#1
cmp col,#85
bleq increment_line

and char,value,#0x00F00000
lsr char,#20
ldrb char,[hex,char]
strb char,[console,col]
add col,#1
cmp col,#85
bleq increment_line

and char,value,#0x000F0000
lsr char,#16
ldrb char,[hex,char]
strb char,[console,col]
add col,#1
cmp col,#85
bleq increment_line

and char,value,#0x0000F000
lsr char,#12
ldrb char,[hex,char]
strb char,[console,col]
add col,#1
cmp col,#85
bleq increment_line

and char,value,#0x00000F00
lsr char,#8
ldrb char,[hex,char]
strb char,[console,col]
add col,#1
cmp col,#85
bleq increment_line

and char,value,#0x000000F0
lsr char,#4
ldrb char,[hex,char]
strb char,[console,col]
add col,#1
cmp col,#85
bleq increment_line

and char,value,#0x0000000F
ldrb char,[hex,char]
strb char,[console,col]
add col,#1
cmp col,#85
bleq increment_line

.unreq value
.unreq char
.unreq console
ldr r0,=Console_Col
str col,[r0]
pop  {r0,r1,r2,r3,r4,pc}








.globl console_write_decimal
console_write_decimal:
// r0 = number to be printed
// Decimal number will be stored in high, med, low in Binary Coded Decimal Form, with 1 digit per 8 bits
// The masks to be added are stored in 'Numbers' with 12 bytes between each power of 2
// Add the low 16 bits of the value, then sort out the carrys, then the high 16 bits
push {r4,r5,r6,r7,r8,r9,r10,lr}
val .req r0
low .req r1
med .req r2
high .req r3
num .req r4
number .req r6
mask .req r7
res .req r9
mov low,#0
mov med,#0
mov high,#0

mov number,r0
mov val,#0
mov mask,#1
cwd_loop_1:
and res,mask,number
cmp res,#0
blne add_val
add val,#1
lsl mask,#1
cmp val,#16
blt cwd_loop_1


// Works from here down :-) (except the bits commented out)

bl sort_rounding


mov val,#16
mov mask,#0x10000

cwd_loop_2:
and res,mask,number
cmp res,#0
blne add_val
add val,#1
lsl mask,#1
cmp val,#32
blt cwd_loop_2
bl sort_rounding
/*
mov low,#0x0000000b
add low,#0x00080000
add low,#0x07000000
mov med,#0x00000006
add med,#0x00000500
add med,#0x00040000
add med,#0x03000000
mov high,#0x02
add high,#0x0100
// */





.unreq number
.unreq res
ptr .req r6
ldr ptr,=Console
ldr r9,=Console_Row
ldr r9,[r9]
mov r10,#NUM_COLS
mla ptr,r9,r10,ptr
ldr r9,=Console_Col
ldr r9,[r9]
add ptr,r9
digit .req r9
and digit,high,#0x0000FF00
lsr digit,#8
add digit,#0x30	// ASCII offset
strb digit,[ptr]
add ptr,#1

and digit,high,#0x000000FF
add digit,#0x30	// ASCII offset
strb digit,[ptr]
add ptr,#1

and digit,med,#0xFF000000
lsr digit,#24
add digit,#0x30	// ASCII offset
strb digit,[ptr]
add ptr,#1

and digit,med,#0x00FF0000
lsr digit,#16
add digit,#0x30	// ASCII offset
strb digit,[ptr]
add ptr,#1

and digit,med,#0x0000FF00
lsr digit,#8
add digit,#0x30	// ASCII offset
strb digit,[ptr]
add ptr,#1

and digit,med,#0x000000FF
add digit,#0x30	// ASCII offset
strb digit,[ptr]
add ptr,#1

and digit,low,#0xFF000000
lsr digit,#24
add digit,#0x30	// ASCII offset
strb digit,[ptr]
add ptr,#1

and digit,low,#0x00FF0000
lsr digit,#16
add digit,#0x30	// ASCII offset
strb digit,[ptr]
add ptr,#1

and digit,low,#0x0000FF00
lsr digit,#8
add digit,#0x30	// ASCII offset
strb digit,[ptr]
add ptr,#1

and digit,low,#0x000000FF
add digit,#0x30	// ASCII offset
strb digit,[ptr]
add ptr,#1

// On entry, r0 is console
// r1 = data
// r2 = col
// r3 = char
//

ldr r1,=Console_Col
ldr r2,[r1]
add r2,#10
cmp r2,#85
subge r2,#85
str r2,[r1]
blge  increment_line
pop {r4,r5,r6,r7,r8,r9,r10,pc}


sort_rounding:
push {lr}
low .req r1
med .req r2
high .req r3
num .req r4
cur .req r0
carry .req r8
new .req r10
reg .req r5
mov mask,#0x000000FF
mov carry,#0

mov reg,low
bl round_4
mov low,reg

mov reg,med
bl round_4
mov med,reg

mov reg,high
bl round_4
mov high,reg

pop {pc}

round_4:
push {lr}
mov new,#0
add reg,carry
mov cur,reg // 0
and cur,mask
bl round_1
add new,cur
ror new,#8
lsr reg,#8
add reg,carry
mov cur,reg // 1
and cur,mask
bl round_1
add new,cur
ror new,#8
lsr reg,#8
add reg,carry
mov cur,reg // 2
and cur,mask
bl round_1
add new,cur
ror new,#8
lsr reg,#8
add reg,carry
mov cur,reg // 3
and cur,mask
bl round_1
add new,cur
ror new,#8
mov reg,new
pop {pc}

round_1:
mask .req r7
res .req r9
cur .req r0
carry .req r8
mov carry,#0
sr_loop:
and res,mask,cur
cmp res,#10
subge cur,#10
addge carry,#1
bge sr_loop
mov pc,lr



add_val:
val .req r0
low .req r1
med .req r2
high .req r3
push {val}
mov r4,#12
mul val,r4
num .req r4
ldr num,=Numbers

ldr r5,[num,val]
add val,#4
add high,r5
ldr r5,[num,val]
add val,#4
add med,r5
ldr r5,[num,val]
add low,r5
pop {val}
mov pc,lr

.section .text
.align 4
.globl Console_Row
Console_Row:
.word 0
Console_Col:
.word 0
Current_num:
.word 0
.word 0
.word 0
Numbers:		// N.B. The comments are the powers of 2, not the numbers
// 0
.word 0x00000000
.word 0x00000000
.word 0x00000001

// 1
.word 0x00000000
.word 0x00000000
.word 0x00000002

// 2
.word 0x00000000
.word 0x00000000
.word 0x00000004

// 3
.word 0x00000000
.word 0x00000000
.word 0x00000008

// 4
.word 0x00000000
.word 0x00000000
.word 0x00000106

// 5
.word 0x00000000
.word 0x00000000
.word 0x00000302

// 6
.word 0x00000000
.word 0x00000000
.word 0x00000604

// 7
.word 0x00000000
.word 0x00000000
.word 0x00010208

// 8
.word 0x00000000
.word 0x00000000
.word 0x00020506

// 9
.word 0x00000000
.word 0x00000000
.word 0x00050102

// 10
.word 0x00000000
.word 0x00000000
.word 0x01000204

// 11
.word 0x00000000
.word 0x00000000
.word 0x02000408

// 12
.word 0x00000000
.word 0x00000000
.word 0x04000906

// 13
.word 0x00000000
.word 0x00000000
.word 0x08010902

// 14
.word 0x00000000
.word 0x00000001
.word 0x06030804

// 15
.word 0x00000000
.word 0x00000003
.word 0x02070608

// 16
.word 0x00000000
.word 0x00000006
.word 0x05050306

// 17
.word 0x00000000
.word 0x00000103
.word 0x01000702

// 18
.word 0x00000000
.word 0x00000206
.word 0x02010404

// 19
.word 0x00000000
.word 0x00000502
.word 0x04020808

// 20
.word 0x00000000
.word 0x00010004
.word 0x08050706

// 21
.word 0x00000000
.word 0x00020009
.word 0x07010502

// 22
.word 0x00000000
.word 0x00040109
.word 0x04030004

// 23
.word 0x00000000
.word 0x00080308
.word 0x08060008

// 24
.word 0x00000000
.word 0x01060707
.word 0x07020106

// 25
.word 0x00000000
.word 0x03030505
.word 0x04040302

// 26
.word 0x00000000
.word 0x06070100
.word 0x08080604

// 27
.word 0x00000001
.word 0x03040201
.word 0x07070208

// 28
.word 0x00000002
.word 0x06080403
.word 0x05040506

// 29
.word 0x00000005
.word 0x03060807
.word 0x00090102

// 30
.word 0x00000100
.word 0x07030704
.word 0x01080204

// 31
.word 0x00000201
.word 0x04070408
.word 0x03060408





.globl Console
Console:
.word 0             // Line 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 1
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 2
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 3
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 4
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 5
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 6
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 7
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 8
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 9
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 10
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 11
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 12
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 13
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 14
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 15
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 16
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 17
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 18
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 19
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 20
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 21
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 22
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 23
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 24
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 25
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 26
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 27
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 28
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 29
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 30
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 31
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 32
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 33
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 34
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 35
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 36
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 37
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 38
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 39
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 40
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 41
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 42
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 43
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 44
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 45
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 46
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0


.word 0             // Line 47
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.byte 0
