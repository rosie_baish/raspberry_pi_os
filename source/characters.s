.equ pixels_per_screen_row, 0x8000	//16 rows per char * 1024 pixels per row * 2 bytes per pixel

/* r0 is column, r1 is row, r2 is value*/
.globl drawAsciiCharacter
drawAsciiCharacter:
push {r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,lr}
col .req r4
row .req r5
val .req r6
buff .req r7

mov col,r0
mov row,r1
mov val,r2

ldr buff,=graphicsAddress
ldr buff,[buff]		// Pointer to framebuffer
ldr buff,[buff,#32]	// Gets the pointer value from the address 32bytes into the framebuffer
add buff,#2		// Character Screen is 2 pixels offset from entire screen
// Move the buffer on to the correct start value
mov r8,#pixels_per_screen_row
mla buff,row,r8,buff
.unreq row
add col,col,lsl #1	// Multiply by 3
add buff,col,lsl #3	// Multiply by 8 and increment (12 pixels per char * 2 bytes per pixel)
.unreq col
// Get the correct char and load the pointer to it into chr
chr .req r8
mov chr,val
asciiTable .req r4
ldr asciiTable,=ascii	// Pointer to byte 0 of the jump table
lsl chr,#2		// 4 bytes / address
add chr, asciiTable	// Pointer to the relevant byte of the jump table
.unreq val
.unreq asciiTable
ldr chr,[chr]		// Pointer to the beginning of the character

// buff now contains the first cell of the area of screen we're writing to, and chr contains the first colour of the char
// To write char, write 12 pixels, the skip the remainder of the row (2024, and repeat 16 times
x .req r9
y .req r10
pix .req r11
mov y,#16
ayloop$:
	mov x,#12	
	axloop$:
		ldrh pix,[chr]
		strh pix,[buff]
		add chr,#2
		add buff,#2
		sub x,#1
		teq x,#0
		bne axloop$ 
	add buff,#1012
	add buff,#1012
	sub y,#1
	teq y,#0
	bne ayloop$
.unreq x
.unreq y
.unreq pix
.unreq buff
.unreq chr
pop  {r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,pc}




/* Render console to the screen */
.globl render
render:
push {r0,r1,r2,r3,r4,r5,r6,lr}
scol .req r0
srow .req r1
sval .req r2
console .req r4

ldr console,=Console
ldr r5,=Console_Row
ldr r5,[r5]
mov srow,#47
sub srow,r5
cmp srow,#48
subge srow,#48
x .req r5
y .req r6

mov y,#48
ldr sval,[console]
colloop$:
	mov x,#85
	mov scol,#0
	rowloop$:
		ldrb sval,[console]
		bl drawAsciiCharacter
		add console,#1
		add scol,#1
		sub x,#1
		teq x,#0
		bne rowloop$
	add srow,#1
	cmp srow,#48
	movge srow,#0
	sub y,#1
	teq y,#0
	bne colloop$
.unreq x
.unreq y

.unreq scol
.unreq srow
.unreq sval
.unreq console
pop {r0,r1,r2,r3,r4,r5,r6,pc}
