.equ system_timer_address, 	0x20003000
.equ compare_0_offset,		0xC
.equ compare_3_offset,		0x18
// 0 and 2 are used by GPU. 3 is for linux, and thus also my OS

/* r0 = number of microseconds to delay */
.globl Wait
Wait:
push {lr}

delay .req r3
timer_address .req r2
hiVal .req r1
loVal .req r0
mov delay,r0
ldr timer_address,=system_timer_address
ldrd loVal,hiVal,[timer_address,#4]	/* load 8 bytes from the address in r2, of which r1 is the large end and r0 is the small */
add delay,loVal
str delay,[timer_address,#compare_0_offset]
.unreq delay
.unreq hiVal
.unreq loVal
status .req r3

_loop$:
	ldr status,[timer_address]
	tst status,#1
beq _loop$
.unreq status
.unreq timer_address

pop {pc}

.globl WaitHalf
WaitHalf:
push {lr}
push {r0,r1,r2,r3}
ldr r0,=500000
bl Wait
pop {r0,r1,r2,r3}
pop {pc}

