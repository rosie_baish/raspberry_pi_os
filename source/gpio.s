.equ no_of_pins,	54
.equ no_of_pin_modes,	8
.equ no_of_pin_states,	2
.equ no_of_irq_states,	2
.equ no_of_pull_up_down_states, 2


.equ clear_reg_offset,	40
.equ set_reg_offset,	28
.equ pull_up_down_control_offset,	0x94
.equ pull_up_down_clock_0_offset,	0x98
.equ rising_edge_offset, 			0x4C
.equ falling_edge_offset,			0x58

.equ ON, 0
.equ OFF, 1
.equ OK_Led_Num, 16

.equ disable_pull_up_down, 	0x00
.equ pull_down, 			0x01
.equ pull_up,				0x10

.equ input,		0b000
.equ output,	0b001
.equ alt_0, 	0b100
.equ alt_1, 	0b101
.equ alt_2, 	0b110
.equ alt_3, 	0b111
.equ alt_4, 	0b011
.equ alt_5, 	0b010

.global Pull_High_Low
Pull_High_Low:
// I'm not sure how, or even if, this works
// Initial attempt:
// r0 = pin number. r1 = 1 for pull high, 0 for pull low

// Validation
cmp r0,#no_of_pins
cmplo r1,#no_of_pull_up_down_states
movhs pc,lr

push {lr}
// Move values to safe registers
pin_num .req r2
high_low .req r3
mov pin_num, r0
mov high_low, r1

mov r1,#input
bl Set_GPIO_Mode

// For GPIO pin 2, 1 is high and 0 is low


GPIO_reg .req r0
ldr r0,=GPIO_Address
ldr GPIO_reg,[r0, #pull_up_down_control_offset]

cmp high_low, #1	// 1 = high
moveq r1,#pull_up
movne r1,#pull_down
str r1,[GPIO_reg]

mov r0,#200		// Wait 150 cycles. Erring on the side of caution with 200
phl_loop_1:
sub r0,#1
cmp r0,#0
bne phl_loop_1

and r0,pin_num,#31
mov r1,#1
lsl r1,r0
ldr r0,=GPIO_Address
ldr GPIO_reg,[r0,#pull_up_down_clock_0_offset]
cmp pin_num,#32
addge GPIO_reg,#4
str r1,[GPIO_reg]

mov r0,#200		// Wait 150 cycles. Erring on the side of caution with 200
phl_loop_2:
sub r0,#1
cmp r0,#0
bne phl_loop_2

ldr r0,=GPIO_Address
ldr GPIO_reg,[r0, #pull_up_down_control_offset]
mov r1,#0
str r1,[GPIO_reg]

add r0,#4	// Move to clock 0
cmp pin_num,#32
addge GPIO_reg,#4		// Pull up/down clock 1 as required
mov r1,#0
str r1,[GPIO_reg]
.unreq GPIO_reg
.unreq pin_num
pop {pc}

.globl Attach_Interrupt
Attach_Interrupt:
// r0 is pin, r1 is rising/falling (0 = rising, 1 = falling)
// Validation
cmp r0,#no_of_pins
cmplo r1,#no_of_irq_states
movhs pc,lr

pin_no .req r2
rise_fall .req r3
mov pin_no, r0
mov rise_fall, r1

GPIO_ptr .req r0
ldr r0,=GPIO_Address
ldr GPIO_ptr,[r0]
cmp pin_no, #32
addge GPIO_ptr, #4
and pin_no, #31

mov r1,#1
lsl r1,pin_no

cmp rise_fall,#0
streq r1,[GPIO_ptr, #rising_edge_offset]
strne r1,[GPIO_ptr, #falling_edge_offset]
.unreq GPIO_ptr
.unreq pin_no
.unreq rise_fall
mov pc,lr

.globl Connect_fiq_gpio
Connect_fiq_gpio:
ldr r0,=0x2000B20C	// FIQ controller
mov r1,#1
lsl r1,#7	// Set bit 7 high to enable
add r1,#49	// gpio_int[0]
str r1,[r0]



/* r0 is pin, r1 is mode */
.globl Set_GPIO_Mode
Set_GPIO_Mode:
cmp r0,#no_of_pins
cmplo r1,#no_of_pin_modes
movhs pc,lr

pin_num .req r2
pin_val .req r1
mov pin_num,r0
GPIO_reg .req r0
ldr r0,=GPIO_Address	// Loads r0 with the pointer to the GPIO_Address
ldr GPIO_reg,[r0]

functionLoop$:
	cmp pin_num,#10
	subhs pin_num,#10
	addhs GPIO_reg,#4
	bhi functionLoop$
/* pin_num = pin_num mod 10 */
/* GPIO_reg += (4 * (pin_num div 0)) */
bitmask .req r3
mov bitmask,#7
lsl bitmask,pin_num

add pin_num,pin_num,lsl #1	// 3 bits per pin
lsl pin_val,pin_num
.unreq pin_num
reg_val .req r2
ldr reg_val,[GPIO_reg]
bic reg_val,bitmask		// reg_val = reg_val AND !bitmask
orr reg_val,pin_val
str reg_val,[GPIO_reg]
.unreq pin_val
.unreq reg_val
.unreq GPIO_reg
mov pc,lr

/* r0 is pin, r1 is mode */
.globl Set_Clear_GPIO
Set_Clear_GPIO:
cmp r0,#no_of_pins
cmplo r1,#no_of_pin_states
movhs pc,lr
pin_val .req r1
pin_num .req r2
mov pin_num,r0
GPIO_reg .req r0
ldr r0,=GPIO_Address	// Loads r0 with the pointer to the GPIO_Address
ldr r0,[GPIO_reg]
teq pin_val,#0
addeq GPIO_reg,#clear_reg_offset
addne GPIO_reg,#set_reg_offset
.unreq pin_val

cmp pin_num,#32
addhs GPIO_reg,#4
and pin_num,#31

set_bit .req r3
mov set_bit,#1
lsl set_bit,pin_num
.unreq pin_num
str set_bit,[GPIO_reg]
.unreq set_bit
.unreq GPIO_reg
mov pc,lr


.globl Flash
Flash:
push {r0,r1,r2,r3,lr}
bl LEDOn
bl WaitHalf
bl LEDOff
bl WaitHalf
pop {r0,r1,r2,r3,pc}


.globl LEDOn
LEDOn:
push {r0,r1,r2,r3,lr}
mov r0,#OK_Led_Num
mov r1,#output
bl Set_GPIO_Mode
mov r0,#OK_Led_Num
mov r1,#ON
bl Set_Clear_GPIO
pop {r0,r1,r2,r3,pc}

.globl LEDOff
LEDOff:
push {r0,r1,r2,r3,lr}
mov r0,#OK_Led_Num
mov r1,#output
bl Set_GPIO_Mode
mov r0,#OK_Led_Num
mov r1,#OFF
bl Set_Clear_GPIO
pop {r0,r1,r2,r3,pc}

.section .data
.align 4
.globl GPIO_Address
GPIO_Address:
.word 0x20200000
