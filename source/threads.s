.section .data
.align 4
.globl thread0
thread0:
.word thread1		// Next
.word thread10		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x7000		// SP
.word 0x0000		// LR
.word 0x0000		// PC

.globl thread1
thread1:
.word thread2		// Next
.word thread0		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x6000		// SP
.word 0x0000		// LR
.word 0x0000		// PC

thread2:
.word thread3		// Next
.word thread1		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x5000		// SP
.word 0x0000		// LR
.word 0x0000		// PC

thread3:
.word thread4		// Next
.word thread2		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x4000		// SP
.word 0x0000		// LR
.word 0x0000		// PC

thread4:
.word thread5		// Next
.word thread3		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x0000		// SP
.word 0x0000		// LR
.word 0x0000		// PC

thread5:
.word thread6		// Next
.word thread4		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x0000		// SP
.word 0x0000		// LR
.word 0x0000		// PC

thread6:
.word thread7		// Next
.word thread5		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x0000		// SP
.word 0x0000		// LR
.word 0x0000		// PC

thread7:
.word thread8		// Next
.word thread6		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x0000		// SP
.word 0x0000		// LR
.word 0x0000		// PC

thread8:
.word thread9		// Next
.word thread7		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x0000		// SP
.word 0x0000		// LR
.word 0x0000		// PC

thread9:
.word thread10		// Next
.word thread8		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x0000		// SP
.word 0x0000		// LR
.word 0x0000		// PC

.globl thread10
thread10:
.word thread0		// Next
.word thread9		// Prev
.word 0x0000		// r0
.word 0x0000		// r1
.word 0x0000		// r2
.word 0x0000		// r3
.word 0x0000		// r4
.word 0x0000		// r5
.word 0x0000		// r6
.word 0x0000		// r7
.word 0x0000		// r8
.word 0x0000		// r9
.word 0x0000		// r10
.word 0x0000		// r11
.word 0x0000		// r12
.word 0x10000		// SP
.word 0x0000		// LR
.word 0x0000		// PC
