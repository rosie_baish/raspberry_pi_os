.equ max_num_tasks,	5	// Due to number of SP locations available

.section .text

.globl add_thread
add_thread:
// Wrapper for the thread pool
// Loads the Head & Tail for both lists into r1-r4
// Returns a pointer to the new thread in r0
push {r4, lr}
ldr r1,=running_head
ldr r2,=running_tail
ldr r3,=spare_head
ldr r4,=spare_tail
bl add_item_circular
pop {r4,pc}

.globl remove_thread
remove_thread:
// Wrapper for the thread pool
// Loads the Head & Tail for both lists into r1-r4
// Returns a pointer to the new thread in r0
push {r4, lr}
ldr r1,=running_head
ldr r2,=running_tail
ldr r3,=spare_head
ldr r4,=spare_tail
bl remove_item_circular
pop {r4,pc}


.globl scheduler_add_task
scheduler_add_task:
// r0 contains initial PC
// r1 - r3 contains initial arguments (will be loaded into r0 - r2)
// Remaining registers will be 0'd
// Returns 0 on success, 1 on failure
push {lr}
push {r0}
push {r1,r2,r3}
bl add_thread
// r0 now contains pointer to thread
pop {r1,r2,r3}
str r1,[r0,#8 ]	// r0
str r2,[r0,#12]	// r1
str r3,[r0,#16]	// r2
mov r3,#0
str r3,[r0,#20]	// r3
str r3,[r0,#24]	// r4
str r3,[r0,#28]	// r5
str r3,[r0,#32]	// r6
str r3,[r0,#36]	// r7
str r3,[r0,#40]	// r8
str r3,[r0,#44]	// r9
str r3,[r0,#48]	// r10
str r3,[r0,#52]	// r11
str r3,[r0,#56]	// r12
pop {r1}
str r1,[r0,#68]	// pc
ldr r1,=scheduler_remove_current_task
str r1,[r0,#64]	// lr
// SP should be already correct
// r3-r12 are undefined as standard
pop {pc}


.globl scheduler_remove_current_task
scheduler_remove_current_task:
// Get current task number
ldr r1,=Current_Task
ldr r0,[r1]	// Base of current task
// Each thread must return it's stack pointer to correct place (same number of pushes as pops)
str sp,[r0,#60]	// Stack Pointer
ldr r1,[r0]		// Next thread so that we know where to go
push {r1}	// Next thread
// Don't have to preserve LR as we know where we're branching next
bl remove_thread
pop {r1}
ldr r0,[r1]		// Base of new task
ldr r1,=Current_Task
str r1,[r0]		// Update Current Task
b jump_into_new_thread


.globl scheduler_next_task
scheduler_next_task:
// Called by the irq handler
// Save values of current task, then jump to 'jump_into_new_thread'"
// Push r0,r1,r2 to give working area
push {r0,r1,r2}
ldr r0,=Program_Counter
ldr r1,[r0]	// Value of PC immediately before interrupt
ldr r0,=Current_Task
ldr r0,[r0]	// Base of current task
str r1,[r0,#68]	// PC
str lr,[r0,#64]	// LR

// store r3 onwards
str r3,[r0,#20]	// r3
str r4,[r0,#24]	// r4
str r5,[r0,#28]	// r5
str r6,[r0,#32]	// r6
str r7,[r0,#36]	// r7
str r8,[r0,#40]	// r8
str r9,[r0,#44]	// r9
str r10,[r0,#48]	// r10
str r11,[r0,#52]	// r11
str r12,[r0,#56]	// r12
// store r0-r2 (which we earlier pushed to give working area)
pop {r3,r4,r5}
str r3,[r0,#8]	// r0
str r4,[r0,#12]	// r1
str r5,[r0,#16]	// r2
// store SP
str sp,[r0,#60]	// SP

// Update current task
ldr r0,=Current_Task
ldr r1,[r0]		// Read Current Task
ldr r1,[r1]		// self.Next
str r1,[r0]		// Write Current Task

// Check whether we need to draw the screen

// Only enable draw_screen when you need the option of debugging once per frame

// bl draw_screen

//Add to number of interrupts
ldr r0,=Interrupts_Since_Last_Screen_Update
ldr r1,[r0]
add r1,#1
str r1,[r0]

// If there have been less than 10, just go to the new thread
ldr r0,=Interrupts_Since_Last_Screen_Update
ldr r1,[r0]
cmp r1,#10
blt jump_into_new_thread

// Else reset and render
mov r1,#0
str r1,[r0]
//mov r0,#0
//bl console_write_hex
//ldr r0,=s_null
//bl console_write_line

bl render

// Jump to 'jump_into_new_thread'
b jump_into_new_thread

.globl jump_into_new_thread
jump_into_new_thread:
// Load task base
ldr r0,=Current_Task
ldr r0,[r0]	// Base of task
// Load new stack pointer
ldr sp,[r0,#60]	// SP
// Get new PC, and push it
ldr r1,[r0,#68]	// PC
push {r1}
// Get r0-r2 and push them
ldr r3,[r0,#8]	// r0
ldr r4,[r0,#12]	// r1
ldr r5,[r0,#16]	// r2
push {r3,r4,r5}
// Get remaining registers, and lr
ldr r3,[r0,#20]	// r3
ldr r4,[r0,#24]	// r4
ldr r5,[r0,#28]	// r5
ldr r6,[r0,#32]	// r6
ldr r7,[r0,#36]	// r7
ldr r8,[r0,#40]	// r8
ldr r9,[r0,#44]	// r9
ldr r10,[r0,#48]	// r10
ldr r11,[r0,#52]	// r11
ldr r12,[r0,#56]	// r12
ldr lr,[r0,#64]	// LR
// Pop r0-r2
pop {r0,r1,r2}
// Pop pc
pop {pc}


.section .data
.align 4
.globl Current_Task
Current_Task:
.word thread0

.globl Number_Of_Tasks // Not including Render & other OS housekeeping
Number_Of_Tasks:
.word 0	// Currently 5 is the max

.globl Program_Counter
Program_Counter:
.word 0

.globl Interrupts_Since_Last_Screen_Update
Interrupts_Since_Last_Screen_Update:
.word 0

//////////////////////////////////////////////////////////////////////
// The thread pool takes the form of 2 doubly linked lists			//
// The first list is running threads, the second is spare threads	//
// The first 4 words are: Running Head, Running Tail,				//
// Spare Head, Spare Tail.											//
// Each thread is laid out as follows: Next, Prev, r0-r12, SP,LR,PC	//
// To add a thread, take the top thread from the spare list,		//
// create it, and then add it to the end of the running list		//
// To remove a thread, do the opposite.								//
//////////////////////////////////////////////////////////////////////
.globl running_head
running_head:
.word 0x0000		// Running Head
.globl running_tail
running_tail:
.word 0x0000		// Running Tail
.globl spare_head
spare_head:
.word thread0		// Spare Head
.globl spare_tail
spare_tail:
.word thread10		// Spare Tail
