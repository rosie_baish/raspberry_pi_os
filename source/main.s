.section .text
.globl main
main:

bl Initialise_Graphics
bl Colour_Screen
bl render
bl Flash

// Add a couple of process that just loop so that the scheduler is busy
ldr r0,=test_thread
mov r1,#0
mov r2,#0
mov r3,#0
bl scheduler_add_task

ldr r0,=test_thread1
mov r1,#0
mov r2,#0
mov r3,#0
bl scheduler_add_task


// Clock interrupts at 1Hz
ldr r0,=9999		// (r0 + 1) = Number of microseconds between interrupts
bl Set_Irq_Timer

b draw_screen

.globl test_thread	// Increments thread_inc by 1 every iteration
test_thread:
ldr r0,=thread_inc
ldr r1,[r0]
add r1,#1
str r1,[r0]
b test_thread

.globl test_thread1	// Increments thread_inc by 1 every iteration
test_thread1:
ldr r0,=thread_inc1
ldr r1,[r0]
add r1,#1
str r1,[r0]
b test_thread1


.globl inf_loop
inf_loop:		// Pause the processor to wait for interrupt to start scheduler
b inf_loop


// Draw_Screen is the main function, called at c. 10Hz to draw the screen. Will eventually include updating console etc.
.globl draw_screen
draw_screen:
push {lr}
ldr r2,=Num_Interrupts
ldr r2,[r2]
mov r0,#52
mov r1,#20
bl writeReg

ldr r2,=Num_Fast_Interrupts
ldr r2,[r2]
mov r0,#52
mov r1,#22
bl writeReg

ldr r5,=0x20003000
ldr r4,[r5]
ldr r3,[r5,#0x14]
ldr r2,[r5,#4]

mov r0,#10
mov r1,#10
bl writeReg

mov r0,#10
mov r1,#12
mov r2,r3
bl writeReg

mov r0,#10
mov r1,#14
mov r2,r4
bl writeReg

ldr r2,=0x7E00B200
ldr r2,[r2]
mov r0,#10
mov r1,#16
bl writeReg

ldr r0,=num_frames
ldr r2,[r0]
add r2,#1
str r2,[r0]
mov r0, #10
mov r1, #18
bl writeReg

ldr r0,=thread_inc
ldr r2,[r0]
mov r0, #20
mov r1, #28
bl writeReg

ldr r0,=thread_inc1
ldr r2,[r0]
mov r0, #20
mov r1, #30
bl writeReg

ldr r2,=Program_Counter
ldr r2,[r2]	// Value of PC immediately before interrupt
mov r0,#20
mov r1,#36
bl writeReg

ldr r5,=Current_Task
ldr r5,[r5]	// Task base
ldr r2,[r5,#0]
mov r0,#22
mov r1,#1
bl writeReg

ldr r2,[r5,#4]
mov r0,#22
mov r1,#2
bl writeReg

ldr r2,[r5,#8]
mov r0,#22
mov r1,#3
bl writeReg

ldr r2,[r5,#12]
mov r0,#22
mov r1,#4
bl writeReg

ldr r2,[r5,#16]
mov r0,#22
mov r1,#5
bl writeReg

ldr r2,[r5,#20]
mov r0,#22
mov r1,#6
bl writeReg

ldr r2,[r5,#24]
mov r0,#22
mov r1,#7
bl writeReg

ldr r2,[r5,#28]
mov r0,#22
mov r1,#8
bl writeReg

ldr r2,[r5,#32]
mov r0,#22
mov r1,#9
bl writeReg

ldr r2,[r5,#36]
mov r0,#22
mov r1,#10
bl writeReg

ldr r2,[r5,#40]
mov r0,#22
mov r1,#11
bl writeReg
bl render

ldr r2,[r5,#44]
mov r0,#22
mov r1,#12
bl writeReg

ldr r2,[r5,#48]
mov r0,#22
mov r1,#13
bl writeReg

ldr r2,[r5,#52]
mov r0,#22
mov r1,#14
bl writeReg

ldr r2,[r5,#56]
mov r0,#22
mov r1,#15
bl writeReg

ldr r2,[r5,#60]
mov r0,#22
mov r1,#16
bl writeReg

ldr r2,[r5,#64]
mov r0,#22
mov r1,#17
bl writeReg

ldr r2,[r5,#68]
mov r0,#22
mov r1,#18
bl writeReg


bl render

b jump_into_new_thread




.globl error
error:			// Error Function. Turns on LED and the loops forever
bl LEDOn
bl WaitHalf
errorint$:
bl Flash
b errorint$		// Remain in loop





.equ no_of_pins,	54
.equ no_of_pin_states,	2


.globl full_input_low
full_input_low:
// Puts current input pins into r2
GPIO_reg .req r0
ldr r0,=GPIO_Address	// Loads r0 with the pointer to the GPIO_Address
ldr GPIO_reg,[r0]
add GPIO_reg,#0x34
ldr r2,[GPIO_reg]
.unreq GPIO_reg
mov pc,lr


.globl non_blocking_input
non_blocking_input:
// r0	gpio pin number
// returns r0 = 1 for high, 0 for low
// Does not check that pin is input
cmp r0,#no_of_pins
movhs pc,lr

pin_num .req r2
mov pin_num, r0

GPIO_reg .req r0
ldr r0,=GPIO_Address	// Loads r0 with the pointer to the GPIO_Address
ldr GPIO_reg,[r0]
add GPIO_reg,#0x34

cmp pin_num, #32
addhs GPIO_reg, #4
and pin_num, #31

bitmask .req r3
mov bitmask,#1
lsl bitmask, pin_num

ldr r2,[GPIO_reg]
and r2,bitmask
lsr r2,pin_num
mov r0,r2

.unreq bitmask
.unreq GPIO_reg
.unreq pin_num
mov pc,lr



.globl blocking_input
blocking_input:
// r0	gpio pin number
// r1	required logic level (0/1)
// Does not check that pin is input
cmp r0,#no_of_pins
cmplo r1,#no_of_pin_states
movhs pc,lr

required_value .req r1
pin_num .req r2
mov pin_num, r0

GPIO_reg .req r0
ldr r0,=GPIO_Address	// Loads r0 with the pointer to the GPIO_Address
ldr GPIO_reg,[r0]
add GPIO_reg,#0x34

cmp pin_num, #32
addhs GPIO_reg, #4
and pin_num, #31

bitmask .req r3
mov bitmask,#1
lsl bitmask, pin_num
lsl required_value, pin_num
.unreq pin_num

blocking_input_read_loop:
	ldr r2,[GPIO_reg]
	and r2,bitmask
	cmp r2,required_value

	bne blocking_input_read_loop

.unreq bitmask
.unreq GPIO_reg
.unreq required_value
mov pc,lr

.section .data
.align 4
.globl num_frames
num_frames:
.word 0
.globl thread_inc
thread_inc:
.word 0
.globl thread_inc1
thread_inc1:
.word 0
