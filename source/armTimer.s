.globl Set_Irq_Timer
Set_Irq_Timer:
// r0 is timer value
push {r0,r1,r2,r3,lr}
mov r2,r0

// Disable timer interrupts
ldr r0,=0x2000B224	// Disable basic Interrupts
mov r1, #1			// Bit 0 is Arm Timer
str r1,[r0]

// Start timer
ldr r0,=0x2000B408	// Arm timer control register
ldr r1,=0x003E0000	// Reset constant for free running counter pre-scaler.
str r1,[r0]

// Amount for  the timer to count down each time.
// Is loaded into the value register whenever the value register reaches 0
// (to give an interrupt at regular intervals)
// The interval between interrupts is (value + 1)microseconds. (so 999 999) gives 1Hz
ldr r0,=0x2000B400	// Arm timer load
str r2,[r0]

ldr r0,=0x2000B418	// Arm timer reload
str r2,[r0]			// Much like timer load. Doesn't immediately trigger the timer to reset. Not sure why this is an advantage

ldr r0,=0x2000B41C	// Arm timer pre-divider
mov r1, #0xF9		// Pre divider value. // Sets how fast the timer counts down. Timer speed = ~250MHz / (pre-divider + 1). Pre divider pf 0xF9 (249) gives a 1MHz clock
str r1,[r0]

ldr r0,=0x2000B40C	// Arm timer clear interrupts
mov r1, #0
str r1,[r0]

ldr r0,=0x2000B408	// Arm timer control register
ldr r1,=0x003E00A2
// 3E is reset counter for free running counter pre-scaler
// A enables both the timer and the timer interrupt
// 2 sets the counter to 23 bits
str r1,[r0]

ldr r0,=0x2000B218	// Enable basic interrupts
mov r1, #1			// Bit 0 is Arm Timer
str r1,[r0]

// Enable timer interrupt
bl irqEnable


pop {r0,r1,r2,r3,pc}
