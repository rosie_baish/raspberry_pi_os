.equ poll_mailbox_offset, 	0x10
.equ sender_mailbox_offset, 	0x14
.equ status_mailbox_offset, 	0x18
.equ config_mailbox_offset, 	0x1C
.equ write_mailbox_offset, 	0x20
.equ number_of_mailboxes, 	16
.equ correct_status_write,	0x80000000
.equ correct_status_read,	0x40000000
.equ mailbox_base_address,	0x2000B880

/* r0 = mailbox number */
.globl Mailbox_Read
Mailbox_Read:

cmp r0,#number_of_mailboxes
movhs pc,lr
push {r4,lr}
mailbox_number .req r4
mov mailbox_number,r0
mailbox_address .req r0
ldr mailbox_address,=mailbox_base_address

status .req r1
read_status_test_loop$:
	ldr status,[mailbox_address,#status_mailbox_offset]
	tst status,#correct_status_read
bne read_status_test_loop$
message .req r2
ldr message,[mailbox_address,#0]

intended_mailbox .req r3
and intended_mailbox,message,#0b1111
teq intended_mailbox,mailbox_number
.unreq intended_mailbox
bne read_status_test_loop$

.unreq status
.unreq mailbox_address
.unreq mailbox_number
and r0,message,#0xfffffff0
.unreq message
pop {r4,pc}



/* r0 = message, mailbox number = r1 */
.globl Mailbox_Write
Mailbox_Write:

tst r0,#0b1111		// Lowest 4 bits of message must be low to facilitate mailbox system
movne pc,lr
cmp r1,#number_of_mailboxes
movhs pc,lr
push {lr}

mailbox_number .req r1
message .req r2
mov message,r0
mailbox_address .req r0
status .req r3
ldr mailbox_address,=mailbox_base_address

	
write_status_test_loop$:
	ldr status,[mailbox_address,#status_mailbox_offset]
	tst status,#correct_status_write
bne write_status_test_loop$
.unreq status

orr message,mailbox_number
.unreq mailbox_number
str message,[mailbox_address,#write_mailbox_offset]
.unreq message
.unreq mailbox_address
pop {pc}
