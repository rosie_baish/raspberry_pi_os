.section .init
.globl _start
_start:
ldr pc, interrupt_handler_pointer_reset
ldr pc, interrupt_handler_pointer_undefined_instruction
ldr pc, interrupt_handler_pointer_software_interrupt
ldr pc, interrupt_handler_pointer_prefetch_abort
ldr pc, interrupt_handler_pointer_data_abort
ldr pc, interrupt_handler_pointer_unused
ldr pc, interrupt_handler_pointer_general
ldr pc, interrupt_handler_pointer_fast

interrupt_handler_pointer_reset:			.word interrupt_handler_reset
interrupt_handler_pointer_undefined_instruction:	.word interrupt_handler_undefined_instruction
interrupt_handler_pointer_software_interrupt:		.word interrupt_handler_software_interrupt
interrupt_handler_pointer_prefetch_abort:		.word interrupt_handler_prefetch_abort
interrupt_handler_pointer_data_abort:			.word interrupt_handler_data_abort
interrupt_handler_pointer_unused:			.word interrupt_handler_reset
interrupt_handler_pointer_general:			.word interrupt_handler_general
interrupt_handler_pointer_fast:				.word interrupt_handler_fast


interrupt_handler_reset:
mov r0, #0x8000
mov r1, #0x0000
ldmia r0!,{r2,r3,r4,r5,r6,r7,r8,r9}
stmia r1!,{r2,r3,r4,r5,r6,r7,r8,r9}
ldmia r0!,{r2,r3,r4,r5,r6,r7,r8,r9}
stmia r1!,{r2,r3,r4,r5,r6,r7,r8,r9}


// set stack in irq mode
	mov r0, #0xd2
	msr cpsr_c, r0
	mov sp, #0x4000

// Set stack in fiq mode
	mov r0, #0xd1
	msr cpsr_c, r0
	mov sp, #0x2000



// come back to superviser mode
	mov r0, #0xd3
    msr cpsr_c, r0
	mov sp, #0x8000

b main


interrupt_handler_software_interrupt:
interrupt_handler_general:
interrupt_handler_undefined_instruction:
interrupt_handler_prefetch_abort:
interrupt_handler_data_abort:
push {r0,r1,r2,r3}
num .req r2
ptr .req r3
ldr ptr,=Num_Interrupts
ldr num, [ptr]
add num,#1
str num,[ptr]
.unreq num
.unreq ptr
ldr r0,=0x2000B40C	// Arm timer clear interrupts
mov r1,#0
str r1,[r0]

// Write the lr (which is the pc immediately before the irq) to the global variable for the scheduler to know where to go back to
ldr r0,=Program_Counter
sub lr,#4
str lr,[r0]
ldr lr,=scheduler_next_task
add lr,#4

pop {r0,r1,r2,r3}
subs pc, lr, #4	// Jumps to first address of scheduler. I think that the s puts it into superviser mode not irq mode

interrupt_handler_fast:
push {r0,r1}
num .req r0
ptr .req r1
ldr ptr,=Num_Fast_Interrupts
ldr num, [ptr]
add num,#1
str num,[ptr]
.unreq num
.unreq ptr
pop {r0,r1}
subs pc,lr,#4	// Next instruction rather than next plus 1. Dont know why it appears to be in supervisor mode


.section .text
.globl irqEnable
irqEnable:
	mrs r0, cpsr
	bic r0,r0,#0xc0		// 0b1100 0000 to set 6th and 7th bit, enabling normal and fast interrupts
	msr cpsr_c, r0		// The _c indicates only the control bits (0-7 are altered
	mov pc, lr


.globl irqDisable
irqDisable:
	mrs r0, cpsr
	mvn r2, $0b10000000
	orr r2, r2, r0
	msr cpsr, r0
	mov pc, lr

.section .data
.align 4
.globl Num_Interrupts
Num_Interrupts:
.word 0x0

.globl Num_Fast_Interrupts
Num_Fast_Interrupts:
.word 0x0


