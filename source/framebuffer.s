.equ max_height, 		2048
.equ max_width, 		2048
.equ max_bit_depth,		32
.equ cache_flush_offset,	0x40000000
.equ mailbox_number,		1

.section .text
.globl Initialise_Framebuffer
Initialise_Framebuffer:

/* Setup and validate inputs */
cmp r0,#max_width
cmpls r1,#max_height
cmpls r2,#max_bit_depth
movhi r0,#0
movhi pc,lr
push {lr}

/*Write inputs into Frame Buffer */
width .req r0
height .req r1
bitDepth .req r2

framebuffer_information .req r3
ldr framebuffer_information,=Framebuffer_Information
str width,	[framebuffer_information,#0]
str height,	[framebuffer_information,#4]
str width,	[framebuffer_information,#8]
str height,	[framebuffer_information,#12]
str bitDepth,	[framebuffer_information,#20]
.unreq width
.unreq height
.unreq bitDepth

mov r0,framebuffer_information
add r0,#cache_flush_offset
mov r1,#mailbox_number
push {framebuffer_information}
.unreq framebuffer_information
bl Mailbox_Write

bl Flash

/* Receive reply from the mailbox */
mov r0,#mailbox_number
bl Mailbox_Read

/* Check to ensure reply == 0. If so, return framebuffer_information, if not return 0 */
cmp r0,#0
pop {r0}
movne r0,#0
pop {pc}


/* Data Section containing the FrameBuffer we require */
.section .data
.align 4	// Lowest 4 bits of the address must be 0 or we cannot send the address
.globl Framebuffer_Information
Framebuffer_Information:
.int 1024	/* #0 	Physical Width 	*/
.int 768	/* #4 	Physical Height	*/
.int 1024	/* #8 	Virtual Width	*/
.int 768	/* #12 	Virtual Height	*/
.int 0		/* #16	GPU - Pitch	*/
.int 24		/* #20	Bit Depth	*/
.int 0		/* #24	X Offset	*/
.int 0		/* #28	Y Offset	*/
.int 0		/* #32	GPU - Pointer	*/
.int 0		/* #36	GPU - Size	*/
