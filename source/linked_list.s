// Each individual linked list will need a wrapper function
// with pointers to it's heads and tails loaded
// remove:
// r0 = Pointer to base of list item to be removed
// add:
// r0 = Pointer of new list item added
// all:
// r1 = Running Head
// r2 = Running Tail
// r3 = Spare Head
// r4 = Spare Tail


.section .text

.globl add_item
add_item:
push {r5,r6,r7,r8,lr}
r_h .req r5
r_t .req r6
s_h .req r7
s_t .req r8
mov r_h,r1
mov r_t,r2
mov s_h,r3
mov s_t,r4
// Get first element of spare list
new .req r0
ldr new,[s_h]
// Check it isn't null - if it is return 0
cmp new,#0
popeq {r5,r6,r7,r8,pc}
// Move head pointer of spare list to second element
spare_second .req r1
ldr spare_second,[new]
str spare_second,[s_h]
// Move prev pointer of second element to NULL
mov r2,#0
str r2,[spare_second,#4]
.unreq spare_second
// Get last element of running list
last_running .req r1
ldr last_running,[r_t]
// Move head pointer of last element to new element
str new,[last_running]
// Move prev pointer of new element to last element
str last_running,[new,#4]
// Move next pointer of new element to NULL
mov r2,#0
str r2,[new]
// Move tail pointer of running list
str new,[r_t]
// Return pointer to new element
.unreq new
.unreq last_running
.unreq r_h
.unreq r_t
.unreq s_h
.unreq s_t
pop {r5,r6,r7,r8,pc}


.globl remove_item
remove_item:
push {r5,r6,r7,r8,lr}
r_h .req r5
r_t .req r6
s_h .req r7
s_t .req r8
old .req r0
mov r_h,r1
mov r_t,r2
mov s_h,r3
mov s_t,r4
// Get prev element of running list
prev .req r1
ldr prev,[old,#4]
// Get next element of running list
next .req r2
ldr next,[old]
// Move prev element's next pointer to next element
str next,[prev]
// Move next element's prev pointer to prev element
str prev,[next,#4]
.unreq next
.unreq prev
// Get last element of spare list
last .req r1
ldr last,[s_t]
// Move next pointer of last element to old element
str old,[last]
// Move prev pointer of old element to last element
str last,[old,#4]
// Move next pointer of old element to NULL
mov r2,#0
str r2,[old]
// Move tail pointer of spare list
str old,[s_t]
// Return
.unreq r_h
.unreq r_t
.unreq s_h
.unreq s_t
.unreq last
.unreq old
pop {r5,r6,r7,r8,pc}




// The same Linked List Operations, but for a circular running list:



.globl add_item_circular
add_item_circular:
push {r5,r6,r7,r8,lr}
r_h .req r5
r_t .req r6
s_h .req r7
s_t .req r8
mov r_h,r1
mov r_t,r2
mov s_h,r3
mov s_t,r4
// Get first element of spare list
new .req r0
ldr new,[s_h]
// Check it isn't null - if it is return 0
cmp new,#0
popeq {r5,r6,r7,r8,pc}
// Move head pointer of spare list to second element
spare_second .req r1
ldr spare_second,[new]
str spare_second,[s_h]
// Move prev pointer of second element to prev pointer of new element
ldr r2,[new,#4]
str r2,[spare_second,#4]
// Move next pointer of last element to second element
last .req r2
ldr last,[new,#4]
str spare_second,[last]
.unreq last
.unreq spare_second
// Get last element of running list
last_running .req r1
ldr last_running,[r_t]
cmp last_running, #0
beq has_no_elements
has_elements:
first .req r2
ldr first,[r_h]		// First Element
// Move head pointer of last element to new element
str new,[last_running]
// Move prev pointer of new element to last element
str last_running,[new,#4]
// Move next pointer of new element to first element
str first,[new]
// Move prev pointer of first element to new element
str new,[first,#4]
// Move prev pointer of new element to last element
str last_running,[new,#4]
b end_if_num_elements

has_no_elements:
str new,[r_h]		// Head
str new,[new]		// Next
str new,[new,#4]	// Prev
end_if_num_elements:
// Move tail pointer of running list
str new,[r_t]
// Return pointer to new element
ldr r0,[r_t]
.unreq new
.unreq last_running
.unreq first
.unreq r_h
.unreq r_t
.unreq s_h
.unreq s_t
pop {r5,r6,r7,r8,pc}


.globl remove_item_circular	// Need to add returning pointers to NULL on all sides
remove_item_circular:
push {r5,r6,r7,r8,lr}
r_h .req r5
r_t .req r6
s_h .req r7
s_t .req r8
old .req r0
mov r_h,r1
mov r_t,r2
mov s_h,r3
mov s_t,r4
// Get prev element of running list
prev .req r1
ldr prev,[old,#4]
// Get next element of running list
next .req r2
ldr next,[old]
// Move prev element's next pointer to next element
str next,[prev]
// Move next element's prev pointer to prev element
str prev,[next,#4]
.unreq next
.unreq prev
// Get last element of spare list
last .req r1
ldr last,[s_t]
// Move next pointer of last element to old element
str old,[last]
// Move prev pointer of old element to last element
str last,[old,#4]
// Move next pointer of old element to NULL
mov r2,#0
str r2,[old]
// Move tail pointer of spare list
str old,[s_t]
// Return
.unreq r_h
.unreq r_t
.unreq s_h
.unreq s_t
.unreq last
.unreq old
pop {r5,r6,r7,r8,pc}
