.equ green,			0x03E0
.equ red,			0xF800
.equ framebuffer_width,		1024
.equ framebuffer_height,	768
.equ framebuffer_bit_depth,	16

.section .data
.align 2
.globl graphicsAddress
graphicsAddress:
.int 0

.section .text

/*
Initialise all of the graphics routines 
*/
.globl Initialise_Graphics
Initialise_Graphics:
push {r4, lr}
mov r0,#framebuffer_width
mov r1,#framebuffer_height
mov r2,#framebuffer_bit_depth
bl Initialise_Framebuffer	// Returns either 0(error) or the pointer to the FrameBuffer
teq r0,#0
beq error
fbuf .req r4
mov fbuf,r0
ldr r4,=graphicsAddress
str r0,[r4]
pop {r4, pc}


/*
Set the entire screen to BackColour
*/
.globl Colour_Screen
Colour_Screen:
push {lr}
buff .req r3
ldr buff,=graphicsAddress
ldr buff,[buff]
ldr buff,[buff,#32]
colour .req r2
mov colour,#red
y .req r1
mov y,#framebuffer_height
x .req r0
drawRow$:
	mov x,#framebuffer_height
	drawPixels$:
		strh colour,[buff]
		add buff,#2
		sub x,#1
		teq x,#0
	bne drawPixels$
	sub y,#1
	teq y,#0
bne drawRow$
.unreq buff
.unreq x
.unreq y
.unreq colour
pop {pc}

/*
r0 = col, r = row
*/
.globl Set_Pixel
Set_Pixel:
col .req r0
row .req r1
buff .req r2
ldr buff,=graphicsAddress
ldr buff,[buff]

cmp row,#framebuffer_height
cmplo col,#framebuffer_width
bhs error

width .req r3
mov width,#framebuffer_width
ldr buff,[buff,#32]
mla col,row,width,col
add buff, col,lsl #1
.unreq row
.unreq width
.unreq col

colour .req r3
mov colour,#green
strh colour,[buff]
.unreq buff
.unreq colour
mov pc,lr
