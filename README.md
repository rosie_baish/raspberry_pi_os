# Raspberry Pi Operating System #

An OS written in ARM Assembly by Rosie Baish, as an educational project on the principles of low level programming, and OS design. The project originally started as a Turing Machine Emulator, and then grew legs. The OS currently (24 Sep 17) has a Console, scheduler, and assembler. The scheduler updates the console on every context switch, allowing each thread to write to it in turn. The assembler can assemble a subset of ARM Assembly, and the result can be executed

### To do List ###
 * Continue C compiler
 * Get hardware interrupts working with GPIO pins
 * Add keyboard support (PS2 keyboard on GPIO pins)
 * Add filesystem support



### Installation guide ###

* Download source
* In main folder run ./install
* Requires sudo password, however it is not necessary to run sudo ./install
* Install script assumes first SD card, may required changing in script file

### Licences ###

* Makefile is the original downloaded from Alex Chadwick's tutorial (see below), and released under LICENCE1
* All source code is original by Rosie Baish, and licenced under GPLv3, in LICENCE2

### Contact ###

* All code by Rosie Baish rosie.baish@gmail.com
* Based upon [tutorial](http://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/os/index.html) by Alex Chadwick awc32@cam.ac.uk
